# TUBES SISTEM TERDISTRIBUSI KELOMPOK 1 

Pada kesempatan kali ini kami dari kelompok 1 akan membuat sebuah aplikasi pengenalan bilangan matematika.
Pada aplikasi ini kami menggunakan metode RPC. RPC atau remote procedure call adalah sebuah metode yang 
memungkinkan kita untuk mengakses sebuah prosedur yang berada di komputer lain. 
Untuk dapat melakukan ini sebuah server harus menyediakan layanan remote procedure.

Jalankan server.py

![server](/uploads/b0d72f3750438a298e03d26923438f31/server.JPG)

Jalankan client.py

![client](/uploads/9eb9ff79fc28b9af78aeb9e15ebd9b1b/client.JPG)

## FITUR UTAMA APLIKASI :

Aplikasi ini memiliki sebuah menu utama yang berisikan :
*  Cek Bilangan Prima 
*  Faktorisasi Bilangan Prima
*  Deret Fibonacci
*  Faktor Persekutuan Terbesar
*  Palindrome
*  Kelipatan Persekutuan Terkecil

## Menu (1) Cek Bilangan :

**Bilangan ganjil/genap**

Menu ini akan melakukan pengecekan angka berdasarkan inputan dari pengguna aplikasi. Output yang dihasilkan
berupa pemberitahuan apakah inputan pengguna merupakan bilangan ganjil atau genap.

![ganjil](/uploads/073a2614497d49e6f9c7a406d21e44d5/ganjil.JPG)

**Bilangan prima**

Menu ini akan melakukan pengecekan angka berdasarkan inputan dari pengguna aplikasi. Output yang dihasilkan
berupa pemberitahuan apakah inputan pengguna merupakan bilangan prima atau bukan.

![prima](/uploads/9b252f16c8159bb1599693abf3dd4b3c/prima.JPG)

## Menu (2) Faktorisasi Bilangan Prima : 

Pada menu yang kedua ini, aplikasi akan melakukan faktorisasi berdasarkan inputan dari pengguna.
Kalau inputan pengguna merupakan bilangan prima, maka aplikasi akan menampilkan hasil faktorisasi dari
bilangan prima tersebut. Jika inputan pengguna bukan bilangan prima, maka aplikasi akan menampilkan pesan bahwa
nilai inputan pengguna bukan bilangan prima.

![faktorprima](/uploads/faca006f521ff81626832d341e889aff/faktorprima.JPG)

## Menu (3) Deret Fibonacci :

Menu ini, aplikasi akan menampilkan seluruh nilai deret fibonacci berdasarkan inputan dari pengguna aplikasi.

![fib](/uploads/87017d773022011fdd0e7c97333eff6a/fib.JPG)

## Menu (4) Faktor Persekutuan Terbesar (FPB) : (NEW)

Menu ini aplikasi akan mencari persekutuan terbesar dua bilangan bulat positif terbesar yang dapat membagi habis kedua bilangan itu.

![4](/uploads/7d472558b804355163619c86ff30c616/4.JPG)

## Menu (5) Palindrome : (NEW)

Menu ini aplikasi Palindrom adalah sebuah kata, frasa, angka maupun susunan lainnya yang dapat dibaca dengan sama baik dari depan maupun belakang.

![5](/uploads/36e3a92275bd069c87748c7dac595860/5.JPG)

## Menu (6) Kelipatan Persekutuan Terkecil (KPK) : (NEW)

Menu ini aplikasi akan mencari bilangan bulat (integer), dan nanti hasil yang diminta adalah bilangan bulat positif terkecil yang bisa dibagi dengan a atau b.

![6](/uploads/1848a9a1fade643625871d88c437801a/6.JPG)