import xmlrpc.client
import socket

s = xmlrpc.client.ServerProxy('http://10.20.2.206:8017')

def bilangan():
	bil = int(input("\nMasukkan angka: "))
	print(s.prime(bil))
	status = input("Anda ingin mengulang? y/n?\n")
    print()
    if status == "y":
        bilangan()
    elif status == "n":
        macambilangan()
	
def faktorisasi():
	bil = int(input('\nMasukkan angka: '))
	print(s.faktorprima(bil))
	menu()
	
def ganjil_genap():
    ganjilgenap = int(input('Masukkan angka : '))
    print(s.add7(int(ganjilgenap)))
    status = input("Anda ingin mengulang? y/n?\n")
    print()
    if status == "y":
        ganjil_genap()
    elif status == "n":
        macambilangan()
        
def kpk():
    bil1 = int(input('Masukkan angka pertama: '))
    bil2 = int(input('Masukkan angka kedua: '))
    print(s.add10(bil1, bil2))
    status = input("Anda ingin mengulang? y/n?\n")
    print()
    if status == "y":
        kpk()
    elif status == "n":
        menu()
	
def fib():
	bil = int(input('\nMasukkan jumlah deret: '))
	n1 = 0
	n2 = 1
	count = 0
	if bil <= 0:
		print("Masukkan bilangan positif")
	elif bil == 1:
		hasil = n1
	else:
		while count < bil:
			print (n1,end=' , ')
			nth = n1 + n2
			n1 = n2
			n2 = nth
			count += 1
	menu()
	
def fpb():
    num1 = int(input('Masukkan angka pertama: '))
    num2 = int(input('Masukkan angka kedua: '))
    print(s.add8(num1, num2))
    status = input("Anda ingin mengulang? y/n?\n")
    print()
    if status == "y":
        fpb()
    elif status == "n":
        menu()
	
def macambilangan():
    pilihan = 0
    while pilihan != 3:
        print("===================================")
        print("1. Cek bilangan ganjil/genap")
        print("2. Cek bilangan prima")
        print("3. Kembali ke menu utama")
        pilihan = input("Masukan pilihan Anda : ")
        if (pilihan == '1'):
            ganjil_genap()
        elif (pilihan == '2'):
            bilangan()
        elif (pilihan == '3'):
            menu()
        else:
            print('Maaf pilihan Anda tidak terdaftar')
            
def pal():
	kata = input("\nMasukkan kata/frasa/angka: ")
	if kata != '':
		print(s.palindrome(kata))
	else:
		print("Inputan tidak boleh kosong!")
		pal()
    status = input("Anda ingin mengulang? y/n?\n")
    print()
    if status == "y":
        kpk()
    elif status == "n":
        menu()

def menu():
	print("\n========= PILIH MENU ========")
	print("1. Macam-macam Bilangan (new)")
	print("2. Faktorisasi Bilangan Prima")
	print("3. Menentukan Deret Fibbonacci")
	print("4. Menentukan Faktor Persekutuan Terbesar (new)")
	print("5. Menentukan Palindrome (new)")
	print("6. Menentukan Kelipatan Persekutuan Terkecil (new)")
	print("7. Keluar")
	print("==============================")
	pilihan = input("Pilih menu: ")

	if pilihan == '1':
		macambilangan()
		
	elif pilihan =='2':
		faktorisasi()
		
	elif pilihan =='3':
		fib()
		
	elif pilihan =='4':
		fpb()
	
	elif pilihan =='5':
		print('\nPalindrom adalah sebuah kata, frasa, angka maupun susunan lainnya yang dapat dibaca dengan sama baik dari depan maupun belakang.')
		pal()
		
	elif pilihan =='6':
	    kpk()
	
	elif pilihan =='7':
	    exit()
		
	else:
		print("===============================")
		print("Inputan salah")
		menu()
		
        
menu()
