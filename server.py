from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_path = ('/RPC2',)

with SimpleXMLRPCServer(("10.20.2.206", 8017), requestHandler=RequestHandler) as server:
	server.register_introspection_functions()
	server.register_function(pow)
	
	def ganjilgenap1(ganjilgenap):
        pesan = ""
        if (ganjilgenap % 2) == 0:
                pesan = "merupakan bilangan genap"
        else:
                pesan = "merupakan bilangan ganjil"
        return str(pesan)
    server.register_function(ganjilgenap1, 'add7')
	
	def hitung_fpb(x,y):
        if x > y:
            smaller = y
        else:
            smaller = x
        for i in range(1, smaller + 1):
            if ((x % i == 0) and (y % i == 0)):
                fpb = i
        return fpb
    server.register_function(hitung_fpb, 'add8')
    
    def hitung_kpk (a,b):
        x = 1
        y = 1
        p = a*x
        q = b*y

        while p != q:
            while p > q :
                y = y+1
                q = b*y
            while p < q:
                x = x + 1
                p = a * x
        if p == q:
            return p
    server.register_function(hitung_kpk, 'add10')

	def prime(angka):
		if angka > 1:
		   for i in range(2,angka):
			   if (angka % i) == 0:
				   hasil = angka,"bukan bilangan prima"
				   break
		   else:
			   hasil = angka,"adalah bilangan prima"
		else:
		   hasil = angka,"bukan bilangan prima"
		return hasil
	server.register_function(prime)
	
	def faktorprima(faktor_prima):
		factorlist = []
		loop = 2
		while loop <= faktor_prima:
			if faktor_prima % loop == 0:
				faktor_prima /= loop
				factorlist.append(loop)
			else:
				loop += 1
		return factorlist
	
	server.register_function(faktorprima)
	
	def fibbonacci(fib):
				
		return hasil
	server.register_function(fibbonacci)
	
	def palindrome(kata):
		panjang_kata = len(kata)
		panjang_kata_array = panjang_kata - 1
		kata_dibalik = ''
		
		for p in range(panjang_kata):
			kata_dibalik += kata[panjang_kata_array - p]
		
		if kata_dibalik == kata:
			hasil = '"{0}" merupakan palindrome'.format(kata)
		else:
			hasil = '"{0}" bukan merupakan palindrome'.format(kata)
				
		return hasil
	server.register_function(palindrome)
	
	print ("\nBerhasil terhubung ke server!")
	server.serve_forever()